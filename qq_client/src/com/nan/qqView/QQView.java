package com.nan.qqView;

import com.nan.service.FileClientService;
import com.nan.service.MessageClientService;
import com.nan.service.UserClientService;
import com.nan.utils.ScannerUtil;
import com.sun.java_cup.internal.runtime.Scanner;
import org.junit.Test;

import java.io.IOException;

public class QQView {

    //显示主菜单
    private boolean loop = true; //控制是否显示菜单
    private String key = "";
    private UserClientService userClientService = new UserClientService();
    private MessageClientService messageClientService = new MessageClientService();
    private FileClientService fileClientService = new FileClientService();

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        new QQView().mainMenu();
        System.out.println("客户退出系统");
    }

    private void mainMenu() throws IOException, ClassNotFoundException {

        while (loop) {
            System.out.println("==============欢迎登录网络通信系统===========");
            System.out.println("\t\t 1 登录系统");
            System.out.println("\t\t 9 退出系统");
            System.out.print("请输入你的选择： ");
            key = ScannerUtil.readString();
            //根据客户输入 来处理不同的逻辑
            switch (key) {
                case ("1"):
                    System.out.print("请输入用户名：");
                    String userId = ScannerUtil.readString();
                    System.out.print("请输入密码：");
                    String pwd = ScannerUtil.readString();
                    //然后这里 需要把用户名和密码 传到服务端进行验证。
                    if (userClientService.checkUser(userId,pwd)) {
                        System.out.println("==============欢迎(用户: " + userId + " 登录成功)===========");
                        while (loop) {
                            System.out.println("==============网络通信系统二级菜单 (用户 " + userId + " )===========");
                            System.out.println("\t\t 1 显示在线用户列表");
                            System.out.println("\t\t 2 群发消息");
                            System.out.println("\t\t 3 私聊消息");
                            System.out.println("\t\t 4 发送文件");
                            System.out.println("\t\t 9 退出系统");
                            System.out.print("请输入你的选择： ");
                            key = ScannerUtil.readString();
                            switch (key) {
                                case ("1"):
                                   userClientService.getAllOnlienUser();
                                    break;
                                case ("2"):
                                    System.out.print("请输入你要群发的话： ");
                                    String mes = ScannerUtil.readString();
                                    messageClientService.sendMessageToAll(mes,userId);
                                    break;
                                case ("3"):
                                    System.out.print("请输入你要私聊的用户号： ");
                                    String getterId = ScannerUtil.readString();
                                    System.out.print("请输入你要说的话： ");
                                    String content = ScannerUtil.readString();
                                    messageClientService.sendMessageToOne(content,userId,getterId);
                                    break;
                                case ("4"):
                                    System.out.println("请输入要发送文件用户的用户名");
                                    String getter = ScannerUtil.readString();
                                    System.out.println("请输入要发送文件的路径 形式： d:\\a.jpg");
                                    String src = ScannerUtil.readString();
                                    System.out.println("请输入要对方接受文件的路径 形式： d:\\a.jpg");
                                    String des = ScannerUtil.readString();
                                    fileClientService.sendFileToOne(userId,getter,src,des);
                                    break;
                                case ("9"):
                                    userClientService.logout();
                                    loop = false;
                                    break;
                            }
                        }
                    } else {
                        System.out.println("登录失败");
                    }
                    break;
                case ("9"):
                    loop = false;
                    break;
            }
        }
    }
}
