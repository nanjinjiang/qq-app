package com.nan.common;

public interface MessageType {
    //定义一些常量 表示不同的消息类型
    String MESSAGE_LOGIN_SUCCEED = "1"; //表示登录成功
    String MESSAFE_LOGIN_FAIL = "2"; //表示登录失败
    String MESSAFE_COMM_MES = "3"; //表示 普通信息包
    String MESSAFE_GET_ALLUSER = "4"; //表示拉取所有在线用户
    String MESSAFE_RETURN_ALLUSER = "5"; //表示返回所有在线用户
    String MESSAFE_CLIENT_EXIT = "6"; //表示客户端请求退出
    String MESSAGE_TOALL_MES = "7";  //表示客户端群发消息
    String MESSAGE_FILE_MES = "8";  //表示客户端发送文件
}
