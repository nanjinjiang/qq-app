package com.nan.service;

import com.nan.common.Message;
import com.nan.common.MessageType;
import com.nan.common.User;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

//该类完成用户登录验证和用户注册等功能
public class UserClientService {

    //将user对象设为属性 是为了 我们有可能在其他地方使用user信息
    private User u = new User();

    //根据userID 和pwd 验证用户是否合法
    public boolean checkUser(String userId, String pwd) throws IOException, ClassNotFoundException {
        boolean b = false;
        u.setPassword(pwd);
        u.setUserId(userId);
        //链接到服务器发送u对象
        Socket socket = new Socket(InetAddress.getLocalHost(), 9999);
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject(u);

        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Message message = (Message) ois.readObject();
        if (message.getMessageType().equals(MessageType.MESSAGE_LOGIN_SUCCEED)) {
            //创建一个 和服务端保持通信的线程
            ClientConnectServerThread clientConnectServerThread = new ClientConnectServerThread(socket);
            clientConnectServerThread.start();
            //为了方便管理 放在线程集合中进行管理
            ManageClientConnectServerThread.addClientConnectServerThread(userId, clientConnectServerThread);
            b = true;
        } else {
            //登录失败
            socket.close();
        }
        return b;
    }

    // 像服务端请求所有的在线用户
    public void getAllOnlienUser() {
        Message message = new Message();
        message.setSender(u.getUserId());
        message.setMessageType(MessageType.MESSAFE_GET_ALLUSER);
        try {
            ObjectOutputStream oos = new ObjectOutputStream
                    (ManageClientConnectServerThread.getClientConnectServerThread(u.getUserId()).getSocket().getOutputStream());
            oos.writeObject(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //退出客户端
    public void logout(){
        Message message = new Message();
        message.setSender(u.getUserId());
        message.setMessageType(MessageType.MESSAFE_CLIENT_EXIT);
        try {
            ObjectOutputStream oos = new ObjectOutputStream
                    (ManageClientConnectServerThread.getClientConnectServerThread(u.getUserId()).getSocket().getOutputStream());
            oos.writeObject(message);
            System.out.println(u.getUserId()+"退出了系统");
            System.exit(0);//结束进程
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
