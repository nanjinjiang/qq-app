package com.nan.service;

import java.util.HashMap;

//该类管理客户端连接到服务器端的线程的类
public class ManageClientConnectServerThread {
   //key 用户id value就是线程
    private static HashMap<String,ClientConnectServerThread> hm = new HashMap();

    //添加线程
    public static void addClientConnectServerThread(String userId,ClientConnectServerThread clientConnectServerThread){
        hm.put(userId, clientConnectServerThread);
    }
    //获取线程
    public static ClientConnectServerThread getClientConnectServerThread(String userId){
        return hm.get(userId);
    }

}
