package com.nan.service;

import com.nan.common.Message;
import com.nan.common.MessageType;

import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;

public class ClientConnectServerThread extends Thread {
    private Socket socket;

    public ClientConnectServerThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
       while(true){
           try {
               System.out.println("客户端链接服务端，等待服务端发送的消息");
               ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
               //如果服务端没有发送消息 线程就回阻塞在这里
               Message message = (Message) ois.readObject();
               if (message.getMessageType().equals(MessageType.MESSAFE_RETURN_ALLUSER)) {
                   System.out.println("\n-------当前在线用户列表------");
                   String[] onlineUsers = message.getContent().split(" ");
                   for (String onlineUser : onlineUsers) {
                       System.out.println("用户：" + onlineUser);
                   }
               } else if (message.getMessageType().equals(MessageType.MESSAFE_COMM_MES)) {
                   System.out.println("\n" + message.getSender() + " 对 " + message.getGetter() +
                           "说" + message.getContent());
               } else if (message.getMessageType().equals(MessageType.MESSAGE_TOALL_MES)) {
                   System.out.println("\n" + message.getSender() + " 对大家说 " +
                           "说" + message.getContent());
               } else if (message.getMessageType().equals(MessageType.MESSAGE_FILE_MES)) {
                   System.out.println("\n" + message.getSender() + " 对" +
                           "发送文件到你的电脑" + message.getDest());
                   FileOutputStream fos = new FileOutputStream(message.getDest());
                   fos.write(message.getFileBytes());
                   fos.close();
                   System.out.println("发送完毕");
               } else {
                   System.out.println("其他类型消息 暂不处理");
               }
           } catch (Exception e) {
               e.printStackTrace();
           }
       }
    }


    //为了方便得到socket
    public Socket getSocket() {
        return socket;
    }
}
