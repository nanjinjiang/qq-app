package com.nan.service;

import com.nan.common.Message;
import com.nan.common.MessageType;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;

public class MessageClientService {

    public void sendMessageToOne(String content, String senderId, String getterId) {
        Message message = new Message();
        message.setContent(content);
        message.setMessageType(MessageType.MESSAFE_COMM_MES);
        message.setGetter(getterId);
        message.setSender(senderId);
        message.setSendTime(new Date().toString());
        System.out.println( "ni对" + getterId + "说：" + content);

        //发送到服务端
        try {
            ObjectOutputStream oos = new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(senderId).getSocket().getOutputStream());
            oos.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessageToAll(String content, String senderId) {
        Message message = new Message();
        message.setContent(content);
        message.setMessageType(MessageType.MESSAGE_TOALL_MES);
        message.setSender(senderId);
        message.setSendTime(new Date().toString());

        //发送到服务端
        try {
            ObjectOutputStream oos = new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(senderId).getSocket().getOutputStream());
            oos.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
