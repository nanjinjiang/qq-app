package com.nan.service;

import com.nan.common.Message;
import com.nan.common.MessageType;

import java.io.*;

public class FileClientService {

    public void sendFileToOne(String sendder ,String getter,String src ,String dest){
        Message message = new Message();
        message.setSender(sendder);
        message.setGetter(getter);
        //源文件
        message.setSrc(src);
        //传输到哪里
        message.setDest(dest);
        message.setMessageType(MessageType.MESSAGE_FILE_MES);

        FileInputStream fis = null;

        byte[] fileBytes =  new byte[(int) new File(src).length()];
        try {
            fis = new FileInputStream(src);
            //将src文件读入到程序的字节数组
            fis.read(fileBytes);
            message.setFileBytes(fileBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n"+sendder+"给"+getter+"发送文件"+src+"到对方的电脑"+dest);
        try {
            ObjectOutputStream oos = new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(sendder).getSocket().getOutputStream());
            oos.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
