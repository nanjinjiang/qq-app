package com.nan.common;

import java.io.File;
import java.io.Serializable;

//客户端和服务端通信是的类型
public class Message implements Serializable {
    //发送者
    private String sender;
    //接收者
    private String getter;
    //发送的内容
    private String content;
    //发送的时间
    private String sendTime;
     //消息类型
    private String messageType;

    //进行扩展 和文件相关的
    private byte[] fileBytes;
    private int fileLen = 0 ;
    private String dest; //将文件传输到哪里
    private String src; //文件的原地址

    public byte[] getFileBytes() {
        return fileBytes;
    }

    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }

    public int getFileLen() {
        return fileLen;
    }

    public void setFileLen(int fileLen) {
        this.fileLen = fileLen;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getGetter() {
        return getter;
    }

    public void setGetter(String getter) {
        this.getter = getter;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }
}
