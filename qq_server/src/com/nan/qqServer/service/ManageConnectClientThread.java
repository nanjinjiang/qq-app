package com.nan.qqServer.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class ManageConnectClientThread {
    private static HashMap<String, ServerConnectClientThread> hm = new HashMap();

    public static HashMap<String, ServerConnectClientThread> getHashMap(){
        return hm;
    }

    public static void addClientThread(String userId, ServerConnectClientThread serverConnectClientThread) {
        hm.put(userId, serverConnectClientThread);
    }

    public static ServerConnectClientThread getClientThread(String userId) {
        return hm.get(userId);
    }

    public static String returnAllUser() {
        Iterator<String> iterator = hm.keySet().iterator();
        String userList = "";
        while (iterator.hasNext()) {
            userList += iterator.next().toString() + " ";
        }
        return userList;
    }

    public static void remove(String userId) {
        hm.remove(userId);
    }
}
