package com.nan.qqServer.service;

import com.nan.common.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

//这个类用来管理离线消息的发送
public class ManageOfflineMsg {

    private static ConcurrentHashMap<String, List<Message>> hashMap = new ConcurrentHashMap();

    public static ConcurrentHashMap<String, List<Message>> returnHashMap (){
        return hashMap;
    }

    public static List<Message> getMessageList(String userId){
        return hashMap.get(userId);
    }

}
