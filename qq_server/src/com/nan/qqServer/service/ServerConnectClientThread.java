package com.nan.qqServer.service;

import com.nan.common.Message;
import com.nan.common.MessageType;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class ServerConnectClientThread extends Thread {
    private Socket socket;
    private String userId;

    public ServerConnectClientThread(Socket socket, String userId) {
        this.socket = socket;
        this.userId = userId;
    }

    //为了方便得到socket
    public Socket getSocket() {
        return socket;
    }

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("服务端和客户端" + userId + "保持通信 ，读取数据");
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                Message message = (Message) ois.readObject();
                if (message.getMessageType().equals(MessageType.MESSAFE_GET_ALLUSER)) {
                    System.out.println(message.getSender() + "要在线用户列表");
                    String allUser = ManageConnectClientThread.returnAllUser();
                    Message message2 = new Message();
                    message2.setMessageType(MessageType.MESSAFE_RETURN_ALLUSER);
                    message2.setGetter(message.getSender());
                    message2.setContent(allUser);
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(message2);
                } else if (message.getMessageType().equals(MessageType.MESSAFE_CLIENT_EXIT)) {
                    System.out.println(message.getSender() + "要退出");
                    ManageConnectClientThread.remove(message.getSender());
                    socket.close();
                    break;
                } else if (message.getMessageType().equals(MessageType.MESSAFE_COMM_MES)) {
                    System.out.println(message.getSender()+"请求私聊消息");
                    //获取 离线信息包
                    ConcurrentHashMap<String, List<Message>> offlineMap = ManageOfflineMsg.returnHashMap();

                    //判断用户是否在线
                    HashMap<String, ServerConnectClientThread> map = ManageConnectClientThread.getHashMap();
                    if (map.get(message.getGetter()) == null){
                        System.out.println(message.getGetter()+"不在线 发送到离线集合包");
                        List<Message> list = new ArrayList();
                        list.add(message);
                        offlineMap.put(message.getGetter(),list);
//                        System.out.println("离线集合包："+offlineMap);
                    }else{
                        ServerConnectClientThread clientThread = ManageConnectClientThread.getClientThread(message.getGetter());
                        ObjectOutputStream oos = new ObjectOutputStream(clientThread.getSocket().getOutputStream());
                        oos.writeObject(message);
                    }

                } else if (message.getMessageType().equals(MessageType.MESSAGE_TOALL_MES)) {
                    System.out.println(message.getSender()+"请求群发消息");
                    HashMap map = ManageConnectClientThread.getHashMap();
                    Iterator<String> iterator = map.keySet().iterator();
                    while (iterator.hasNext()){
                        String userId = iterator.next();
                        if(!userId.equals(message.getSender())){
                            ServerConnectClientThread clientThread = ManageConnectClientThread.getClientThread(userId);
                            ObjectOutputStream oos = new ObjectOutputStream(clientThread.getSocket().getOutputStream());
                            oos.writeObject(message);
                        }
                    }
                } else if (message.getMessageType().equals(MessageType.MESSAGE_FILE_MES)) {
                    System.out.println(message.getSender()+"请求发送文件");
                    ServerConnectClientThread clientThread = ManageConnectClientThread.getClientThread(message.getGetter());
                    ObjectOutputStream oos = new ObjectOutputStream(clientThread.getSocket().getOutputStream());
                    oos.writeObject(message);
                }

                else {
                    System.out.println("其他类型的消息暂不处理");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
