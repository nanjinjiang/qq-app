package com.nan.qqServer.service;

import com.nan.common.Message;
import com.nan.common.MessageType;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

//服务端推送新闻线程
public class NewsToAllThread implements Runnable {

    @Override
    public void run() {
      //为了多次推送 用while 循环
        while (true){
            System.out.println("请输入服务器要推送的新闻/消息 【输入exit退出】");
            Scanner scanner = new Scanner(System.in);
            String news = scanner.next();
            if ("exit".equals(news)){
                break;
            }
            Message message = new Message();
            message.setSender("服务器");
            message.setSendTime(String.valueOf(new Date()));
            message.setContent(news);
            message.setMessageType(MessageType.MESSAGE_TOALL_MES);
            System.out.println("服务器向客户端推送消息：" + news);
            //遍历所有的通信线程 得到socket 并发送消息
            HashMap<String, ServerConnectClientThread> hm = ManageConnectClientThread.getHashMap();
            Iterator<String> iterator = hm.keySet().iterator();
            while (iterator.hasNext()) {
                String onLineUserId = iterator.next();
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(hm.get(onLineUserId).getSocket().getOutputStream());
                    oos.writeObject(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }

}
