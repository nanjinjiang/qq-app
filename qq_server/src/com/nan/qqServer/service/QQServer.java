package com.nan.qqServer.service;

import com.nan.common.Message;
import com.nan.common.MessageType;
import com.nan.common.User;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class QQServer {
    private ServerSocket ss = null;
    //创建一个集合 如果是这些用户登录 就认为是合法的
    //线程安全的ConcurrentHashMap
    private  static ConcurrentHashMap<String ,User> userHashMap = new ConcurrentHashMap<>();
    static {
        userHashMap.put("100",new User("100","123456"));
        userHashMap.put("至尊宝",new User("至尊宝","123456"));
        userHashMap.put("紫霞仙子",new User("紫霞仙子","123456"));
        userHashMap.put("菩提老祖",new User("菩提老祖","123456"));
    }
    //验证用户
    public boolean checkUser(String userId,String password){
        User user = userHashMap.get(userId);
        if (user==null){
            return false;
        }
        if (!user.getPassword().equals(password)){
            return false;
        }
        return true;
    }
    public QQServer() {
        try {
            ss = new ServerSocket(9999);
            System.out.println("服务端在9999端口监听");
            new Thread(new NewsToAllThread()).start();

            //当和某个客户端连接后 会继续监听 所以要用while
            while (true) {
                Socket socket = ss.accept();
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                User u = (User) ois.readObject();
                Message message = new Message();
                //验证
                if (checkUser(u.getUserId(),u.getPassword())) {
                    message.setMessageType(MessageType.MESSAGE_LOGIN_SUCCEED);
                    //回复
                    oos.writeObject(message);

                    //创建一个线程 和客户端保持通信
                    ServerConnectClientThread serverConnectClientThread = new ServerConnectClientThread(socket, u.getUserId());
                    serverConnectClientThread.start();
                    ManageConnectClientThread.addClientThread(u.getUserId(), serverConnectClientThread);

                    //检测离线消息包是否有数据
                    List<Message> messageList = ManageOfflineMsg.getMessageList(u.getUserId());
                    System.out.println(messageList);
                    if(messageList != null){
                        for (Message message1 : messageList) {
                            ObjectOutputStream oos2 = new ObjectOutputStream(ManageConnectClientThread.getClientThread(message1.getGetter()).getSocket().getOutputStream());
                            oos2.writeObject(message1);
                        }
                    }

                } else {
                    System.out.println("id:" +u.getUserId()+" 密码"+u.getPassword()+ "验证失败");
                    //登录失败
                    message.setMessageType(MessageType.MESSAFE_LOGIN_FAIL);
                    //回复
                    oos.writeObject(message);
                    socket.close();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
          //如果 服务端退出了While 说明 服务器不在监听
            try {
                ss.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
