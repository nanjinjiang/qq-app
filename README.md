# qq-App

#### 介绍
Socket+多线程+IO 实现的一个qq客户端

#### 软件架构
分为QQ客户端和服务端


#### 功能实现

1.  展示所以的在线用户
2.  私聊功能（包括离线和在线用户）
3.  发送文件
4.  退出系统
5.  群发消息

#### 使用说明

1.  直接运行main方法即可
2.  可开启多个客户端 多用户在线聊天


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
